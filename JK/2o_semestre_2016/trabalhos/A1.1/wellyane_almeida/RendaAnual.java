/*
 * Programa irá calcular a renda anual em cima do salario da pessoa.
 */
package rendaanual;

/**
 * @author: Wellyane Almeida dos Santos
 */
import javax.swing.JOptionPane;

public class RendaAnual {

	public static void main(String[] args){
	            /*Aqui digita a renda */
			double renda = Double.parseDouble(JOptionPane.showInputDialog("Digite sua renda"));
		/*Renda menor ou igual a 2000 não paga imposto*/
		if (renda <= 2000){
			JOptionPane.showInputDialog(null, "Não paga imposto");
		}
		/*Renda maior que 2000 ou menor ou igual a 5000 paga imposto*/
		else if (renda > 2000 && renda <= 5000){
			double impostoPrimario = 0.05;
			double valorSobImposto = renda * impostoPrimario;
			double totalAPagar = renda + valorSobImposto;
			JOptionPane.showMessageDialog(null, "Imposto primáro: "+ totalAPagar );
		}
		/*Renda maior que 5000 ou menor ou igual a 10000 paga imposto*/
		else if (renda > 5000 && renda <= 10000){
			double impostoSegundario = 0.10;
			double valorSobImposto = renda * impostoSegundario;
			double totalAPagar = renda + valorSobImposto;
			JOptionPane.showMessageDialog(null, "Imposto segundario: "+ totalAPagar );
	}

	}
}
